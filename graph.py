# __author__ = 'Pawel Polit'

import numpy as np


class Graph(object):
    def __init__(self, dissimilarity_matrix, proximity_matrix):
        self.__dissimilarity_matrix__ = np.copy(dissimilarity_matrix)
        self.__proximity_matrix__ = np.copy(proximity_matrix)
        self.__weights_matrix__ = self.__dissimilarity_matrix__ * self.__proximity_matrix__
        self.__saliency_first__ = None
        self.__saliency_second__ = None

    def transform_to_second(self, saliency_first=None):
        if saliency_first is not None:
            self.__saliency_first__ = saliency_first

        saliency_to_use = self.__saliency_first__.reshape(1, len(self.__saliency_first__))
        saliency_matrix = saliency_to_use.T.dot(saliency_to_use)

        self.__weights_matrix__ = (1 - self.__dissimilarity_matrix__) * self.__proximity_matrix__ * saliency_matrix

    def merw(self):
        eigenvalues, eigenvectors = np.linalg.eig(self.__weights_matrix__)
        max_eigenvector = eigenvectors[:, eigenvalues.argmax()]
        saliency = np.square(max_eigenvector)

        if self.__saliency_first__ is None:
            self.__saliency_first__ = saliency
        else:
            self.__saliency_second__ = saliency

        return np.copy(saliency)
