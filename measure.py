# __author__ = 'Pawel Polit'

import numpy as np


class Dissimilarity(object):
    def __init__(self, histograms, colors_distance_matrix):
        self.__histograms__ = np.copy(histograms)
        self.__colors_distance_matrix__ = np.copy(colors_distance_matrix)
        self.__computed__ = {}

    def compute(self, i, j):
        if (i, j) in self.__computed__:
            return self.__computed__[(i, j)]

        result = self.__histograms__[i].dot(self.__colors_distance_matrix__).dot(self.__histograms__[j])

        self.__computed__[(i, j)] = result
        self.__computed__[(j, i)] = result

        return result

    def prepare_matrix(self):
        number_of_superpixels = len(self.__histograms__)

        dissimilarity_matrix = np.zeros((number_of_superpixels, number_of_superpixels))

        for i in xrange(number_of_superpixels):
            for j in xrange(i + 1):
                dissimilarity_matrix[i][j] = self.compute(i, j)
                dissimilarity_matrix[j][i] = dissimilarity_matrix[i][j]

        return dissimilarity_matrix


class Proximity(object):
    def __init__(self, centroids_distance_matrix, sigma_s_2=0.4):
        self.__results__ = np.exp((-1.0) * np.square(centroids_distance_matrix) / sigma_s_2)

    def compute(self, i, j):
        return self.__results__[i][j]

    def prepare_matrix(self):
        return np.copy(self.__results__)
