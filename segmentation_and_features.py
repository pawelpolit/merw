# __author__ = 'Pawel Polit'

import cv2
import numpy as np
from sklearn.cluster import MiniBatchKMeans


class ImageProcessor(object):
    def __init__(self, image_name):
        self.__image__ = cv2.imread('input/' + image_name)
        self.__seeds__ = None

    def color_quantization(self, k):
        image_lab = cv2.cvtColor(self.__image__, cv2.COLOR_BGR2LAB)
        pixels_list = image_lab.reshape((image_lab.shape[0] * image_lab.shape[1], 3))

        cls = MiniBatchKMeans(n_clusters=k)
        labels = cls.fit_predict(pixels_list)
        quantized_image = cls.cluster_centers_.astype('uint8')[labels]

        image_lab = quantized_image.reshape((image_lab.shape[0], image_lab.shape[1], 3))

        self.__image__ = cv2.cvtColor(image_lab, cv2.COLOR_LAB2BGR)

        return self

    def superpixels(self, region_size):
        image_hsv = cv2.cvtColor(self.__image__, cv2.COLOR_BGR2HSV)

        self.__seeds__ = cv2.ximgproc.createSuperpixelSLIC(image_hsv, region_size=region_size)
        self.__seeds__.iterate()

        return self

    def show_image(self, with_superpixels=False):
        image_to_show = self.__image__

        if with_superpixels:
            height, width, channels = image_to_show.shape

            color_img = np.zeros((height, width, channels), np.uint8)
            color_img[:] = (0, 0, 255)

            mask = self.__seeds__.getLabelContourMask(False)
            mask_inv = cv2.bitwise_not(mask)
            image_to_show_bg = cv2.bitwise_and(image_to_show, image_to_show, mask=mask_inv)
            image_to_show_fg = cv2.bitwise_and(color_img, color_img, mask=mask)
            image_to_show = cv2.add(image_to_show_bg, image_to_show_fg)

        cv2.namedWindow('image')
        cv2.moveWindow('image', 500, 0)
        cv2.imshow('image', image_to_show)
        cv2.waitKey(0)

        return self

    def extract_features(self):
        image_lab = cv2.cvtColor(self.__image__, cv2.COLOR_BGR2LAB)
        pixels_list = image_lab.reshape((image_lab.shape[0] * image_lab.shape[1], 3))
        image_colors = np.vstack({tuple(pixel) for pixel in pixels_list})

        number_of_superpixels = self.__seeds__.getNumberOfSuperpixels()
        superpixel_image = self.__seeds__.getLabels()

        histograms = np.zeros((number_of_superpixels, len(image_colors)))

        height, width = image_lab.shape[:2]

        for row in xrange(height):
            for column in xrange(width):
                color_index = np.where(np.all(image_colors == image_lab[row][column], axis=1))[0][0]
                histograms[superpixel_image[row][column]][color_index] += 1.0

        colors_distance_matrix = self.__compute_distance_matrix__(image_colors)

        centroids_distance_matrix = self.__compute_centroids_distance_matrix__(superpixel_image, number_of_superpixels)

        return histograms, colors_distance_matrix, centroids_distance_matrix

    def show_saliency_map(self, saliency_map, file_name):
        saliency_map_tmp = saliency_map - saliency_map.min()
        saliency_map_transformed = saliency_map_tmp * (1.0 / saliency_map_tmp.max())

        superpixel_image = self.__seeds__.getLabels()

        image_to_show = saliency_map_transformed[superpixel_image]

        cv2.namedWindow('saliency')
        cv2.moveWindow('saliency', 1100, 0)
        cv2.imshow('saliency', image_to_show)

        if cv2.waitKey(0) == 1048586:
            cv2.imwrite('result/' + file_name, 255 * image_to_show)

    @staticmethod
    def __compute_distance_matrix__(array):
        distance_matrix = np.zeros((len(array), len(array)))

        for i in xrange(len(array)):
            for j in xrange(i + 1):
                distance_matrix[i][j] = np.linalg.norm(array[i] - array[j])
                distance_matrix[j][i] = distance_matrix[i][j]

        return distance_matrix

    @staticmethod
    def __compute_centroids_distance_matrix__(superpixel_image, number_of_superpixels):
        centroids = ImageProcessor.__compute_superpixels_centroids__(superpixel_image, number_of_superpixels)
        centroids_distance_matrix = ImageProcessor.__compute_distance_matrix__(centroids)

        shifted_matrix = centroids_distance_matrix - np.min(centroids_distance_matrix)
        return shifted_matrix / np.max(shifted_matrix)

    @staticmethod
    def __compute_superpixels_centroids__(superpixel_image, number_of_superpixels):
        superpixels_centroids = []

        for superpixel in xrange(number_of_superpixels):
            superpixels_centroids.append(np.mean(np.c_[np.where(superpixel_image == superpixel)], axis=0))

        return np.array(superpixels_centroids)
