# __author__ = 'Pawel Polit'

from segmentation_and_features import ImageProcessor
from measure import Dissimilarity, Proximity
from graph import Graph

image_name = '12477.jpg'

image_processor = ImageProcessor(image_name).show_image().color_quantization(8).show_image().superpixels(region_size=80).show_image(with_superpixels=True)
histograms, colors_distance_matrix, centroids_distance_matrix = image_processor.extract_features()

dissimilarity = Dissimilarity(histograms, colors_distance_matrix)
proximity = Proximity(centroids_distance_matrix)

graph = Graph(dissimilarity.prepare_matrix(), proximity.prepare_matrix())
saliency_first = graph.merw()

image_processor.show_saliency_map(saliency_first, image_name)

graph.transform_to_second()
saliency_second = graph.merw()

image_processor.show_saliency_map(saliency_second, image_name)
